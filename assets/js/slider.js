// https://github.com/woothemes/FlexSlider/wiki/FlexSlider-Properties
$(function() {
	$('.flexslider').flexslider({
		slideshow: false,
		animation: "slide",
		easing: "ease",
		controlNav: true,
		slideshowSpeed: 7000,
		prevText: "",
		nextText: "",
		startAt: 0,

		// Usability
		pauseOnHover: true,
		pauseOnAction: true,
		touch: true,
		useCSS: true,
		video: false,
	});

	sliderCaption();
});

function sliderCaption() {

	// DOM cache
	var $slider = $('.flexslider'),
		$caption = $('.flex-caption');

	$slider.on('mouseenter',function() {
		$caption.fadeIn();
	}).on('mouseleave',function() {
		$caption.fadeOut();
	})
}