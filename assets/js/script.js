$(function() {
	autocomplete();
	toggleSearch();
	toggleCart();
	toggleDropdown();
	closeDropdown();
	resizeHotPick();
	showNavigation();
	navigationDropdown();
	addToBag();
	activeColor();

	if($(window).width() >= 1025) {
		showDate();
	}

	$(window).resize(function() {
		resizeHotPick();

		if($(window).width() >= 1025) {
			showDate();
		}

		if($(window).width() >= 768) {
			if($('.site-wrapper').hasClass('open')) {
				$('.site-wrapper').removeClass('open');
			}
		}
	});
});

function toggleSearch() {
	
	// DOM cache
	var $trigger = $('.search-trigger'),
		$block = $('.search-block'),
		$wrapper = $('.site-wrapper');

	$trigger.on('click',function(){
		$block.slideToggle();

		if($wrapper.hasClass('open')) {
			$wrapper.removeClass('open');
		}
	});
}

function toggleCart() {

	// DOM cache
	var $trigger = $('.cart-trigger'),
		$block = $('.cart-wrapper');

	$trigger.on('click', function() {
		$block.slideToggle();
	});
}

function toggleDropdown() {

	// DOM cache
	var $container = $('.dropdown-container'),
		$trigger = $('.drop-trigger'),
		$close = $('.drop-close'),
		$drop = $('.dropdown');

	$trigger.on('click',function(){

		var $this = $(this).siblings('.dropdown');


		// Hide all dropdowns
		$container.find('.dropdown').slideUp();
		
		// Check if dropdown is visible
			// true - hide dropdown
			// false - show dropdown
		if($this.is(':visible')) {
			$this.slideUp();
		}

		else {
			$this.delay(500).slideDown();
		}
	});

	$close.on('click',function(){
		$drop.slideUp();
	});
}

function navigationDropdown() {

	// DOM cache
	var $trigger = $('.nav-dropdown-trigger'),
		$container = $('.mobile-navigation');

	$trigger.on('click', function() {

		var $this = $(this).siblings('.nav-dropdown'),
			$close = $(this).children('.close'),
			$arrow = $(this).children('.arrow-down-b');

		$container.find('.nav-dropdown').slideUp();

		if($this.is(':visible')) {
			$this.slideUp();
			$close.hide(500, function() {
				$arrow.show();
			});
		}

		else {
			$this.delay(500).slideDown();
			$arrow.hide(500, function() {
				$close.show();
			});
		}
	});
}

function closeDropdown() {

	// If click is outside of dropdown-container, close all dropdowns
	$(document).on('click',function(event){

		var $target = $(event.target),
			$container = $('.dropdown-container'),
			$dropdown = $('.dropdown');

		if(!$container.has($target).length > 0) {
			$dropdown.slideUp();
		}
	});
}

function autocomplete() {

	// Array
	var availableTags = ["Black shirt", "Red shirt", "White shirt", "Blue shirt", "Pink shirt", "Purple shirt", "Red pants", "Blue pants", "Black pants"];

	// DOM cache
	var $input = $("#search");

		$input.autocomplete({
			minLength: "",
			source: availableTags,
			appendTo: ".results"
		});
}

function showDate() {

	// DOM cache
	var $container = $('.site-title'),
		$title = $container.children('h1'),
		$date = $('#date');

	// Month and day arrays
	var monthNames = [ "January", "February", "March", "April", "May", "June",
    "July", "August", "September", "October", "November", "December" ];
	var dayNames= ["Monday","Tuesday","Wednesday","Thursday","Friday","Saturday", "Sunday"];

	// Setting the date
	var newDate = new Date();
	newDate.setDate(newDate.getDate());
	$date.text(newDate.getDate() + ' ' + monthNames[newDate.getMonth()] + ' ' + newDate.getFullYear()); // Show (Day, Month name, Year)

	// On hover hide title, show date
	$container.on('mouseenter',function() {
		$title.hide(400, function() {
			$date.show();
		});
	}).on('mouseleave',function() {
		$date.hide(400, function() {
			$title.show();
		});
	});
}

function resizeHotPick() {

	// DOM cache
	var $figure = $('.shop'),
		figureWidth = $figure.width();

	$figure.css({'height': figureWidth + 'px'});
}

function showNavigation() {

	// DOM cache
	var $trigger = $('.navigation-trigger'),
		$wrapper = $('.site-wrapper'),
		$searchTrigger = $('.search-trigger'),
		$searchBlock = $('.search-block');

	$trigger.on('click', function() {
		$wrapper.toggleClass('open');

		// Close search block if open
		if($searchBlock.is(':visible')) {
			$searchBlock.slideUp();
		}
	});
}

function addToBag() {

	// DOM cache
	var $button = $('.add-bag');

	$button.on('click', function() {
		$(this).text("Added to bag");
		$(this).children('bag').hide();
	});
}

function activeColor() {

	// DOM cache
	var $container = $('.color'),
		$color = $('.colors');

	$color.on('click', function() {

		if($(this).siblings('.colors').hasClass('active-color')) {
			$(this).siblings('.colors').removeClass('active-color');
		}

		$(this).addClass('active-color');
	});
}